/** @format */

import axios from 'axios';
import { BOOKING_URI } from '../config/config';
let token = JSON.parse(localStorage.getItem('token'));
const config = {
  headers: {
    Authorization: `Bearer ${token?.access_token}`,
  },
};

export const Get_All_Rentals = () => {
  return axios.get(`${BOOKING_URI}rentals?include=photos`, config);
};
export const Get_Rentals_Include = (include) => {
  return axios.get(`${BOOKING_URI}rentals?include=` + include, config);
};
export const Get_Rental_ById_Include = (id) => {
  return axios.get(`${BOOKING_URI}rentals/` + id, config);
};
export const Get_All_RentalsByPage = (page, include) => {
  return axios.get(
    `${BOOKING_URI}rentals?include=` + include + `&page=` + page,
    config
  );
};

export const Get_All_Photo = () => {
  return axios.get(`${BOOKING_URI}photos`, config);
};
