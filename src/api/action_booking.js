/** @format */

import axios from 'axios';
import { BOOKING_URI } from '../config/config';
let token = JSON.parse(localStorage.getItem('token'));
const config = {
  headers: {
    Authorization: `Bearer ${token?.access_token}`,
  },
};

export const Get_Bookings = () => {
  return axios.get(`${BOOKING_URI}bookings?include=bookings_fees`, config);
};
export const Get_BookingsInclude = (include) => {
  return axios.get(`${BOOKING_URI}bookings?include=` + include, config);
};
export const Get_BookingsInclude_ByRentalId = (RentalId, include) => {
  return axios.get(
    `${BOOKING_URI}bookings?rental_id=` + RentalId + `&include=` + include,
    config
  );
};
export const Get_BookingsInclude_ByRentalId_Page = (
  RentalId,
  include,
  page
) => {
  return axios.get(
    `${BOOKING_URI}bookings?rental_id=` +
      RentalId +
      `&include=` +
      include +
      `&page=` +
      page,
    config
  );
};
//FIlter (rentalId,include,page,status,from enddate,until startdate)
export const Get_BookingsInclude_FIlter_Page = (RentalId,include,page,status,dateDebut,dateFin,months) => {
  return axios.get(`${BOOKING_URI}bookings?rental_id=`+RentalId+`&include=`+include+`&page=`+page+`&status=`+status+`&from=`+dateDebut+`&until=`+dateFin+`&months=`+months, config);
};

export const Get_Fees_By_Booking = (fee) => {
  return axios.get(`${BOOKING_URI}bookings_fees/` + fee, config);
};

export const Get_Fees_By_Booking_Sources = () => {
  return axios.get(`${BOOKING_URI}sources/`, config);
};
