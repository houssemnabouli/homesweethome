/** @format */

import React, { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Grid from '@mui/material/Grid';
import axios from 'axios';
import Button from '@mui/material/Button';
import Card from '../../components/Card';
import { PDFDownloadLink } from '@react-pdf/renderer';
import PdfDocument from './Invoice/Invoice';
import Test from './Invoice/Test';
import data from './Invoice/JsonData/generated.json';
import Datepickers from './DatePicker';
import { PDFViewer } from '@react-pdf/renderer';

import {
  Get_Bookings,
  Get_BookingsInclude,
  Get_BookingsInclude_ByRentalId,
  Get_BookingsInclude_ByRentalId_Page,
  Get_Fees_By_Booking_Sources,
  Get_BookingsInclude_FIlter_Page
} from '../../api/action_booking';

import Calenders from '../../views/uikit/calender';
import { rectangle } from 'leaflet';
let Fulllist = [];

function Details() {
  const [nameFilter, setNameFilter] = useState('');
  let history = useHistory();
  const location = useLocation();
  const [select, SetSelect] = useState('');
  const [rental, setRental] = useState();
  const [listBookings, setListBookings] = useState([]);
  const [mindDate, setMinDate] = useState('');
  const [sources, setSources] = useState([]);
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  //Handles for various components
  const handleFacture = () => {
    history.push('/facture', { state: { bookings: listBookings } });
  };
  useEffect(() => {
    setRental(location.state.state.rental);
    if (rental != null) {
      Get_BookingsInclude_ByRentalId(
        rental,
        'bookings_fees,sources,bookings_payments'
      ).then((res) => {
        Fulllist.push(res.data.bookings);
        setListBookings(res.data.bookings);
        setMinDate(res.data.bookings[0].start_at.substr(0, 10));
      });
    }
  }, [rental]);
  const getDates = (start, end) => {
    console.log("called start:", start, "end:", end)
    setStartDate(start)
    setEndDate(end)
  }
  useEffect(() => {

    if (endDate != null && startDate != null && rental != null) {
      //FIlter (rentalId,include,page,status,from enddate,until startdate)
      console.log("rental", rental)
      let aux = []
      Get_BookingsInclude_FIlter_Page(rental, "bookings_fees,sources,bookings_payments", 1, "booked,Tentative,Unavailable", startDate, endDate, "").then((res) => {
        Array.prototype.push.apply(aux, res.data.bookings);
       
        setListBookings(aux)
      })
      console.log(aux)

    }
  }
    , [ endDate])
  useEffect(() => {
    Get_Fees_By_Booking_Sources().then((res) => {
      console.log(res.data, 'res.data');
      setSources(res.data.sources);
    });
  }, []);
  return (
    <Grid container sx={{ my: 5 }} spacing={2}>
      <Grid item md={12}>
        <Card>
          <Card.Header className='d-flex justify-content-between'></Card.Header>
          <Card.Body className='px-0'></Card.Body>
          <Grid
            container
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Grid item md={3}>
              <div class='form-floating mb-3'>
                <input
                  type='texte'
                  class='form-control'
                  id='floatingInput'
                  value={nameFilter}
                  onChange={(e) => {
                    setNameFilter(e.target.value);
                  }}
                  placeholder='name@example.com'></input>
                <label for='floatingInput'>Nom de location</label>
              </div>
            </Grid>

            <Grid item md={3}>
              <Calenders getDates={getDates} minDate={mindDate}></Calenders>
            </Grid>
            <Grid item md={3}>
              paiement manquant
            </Grid>
          </Grid>
        </Card>
      </Grid>
      <Grid item md={12}>
        <Card>
          <Card.Header className='d-flex justify-content-between'>
            <Grid
              container
              direction='row'
              justifyContent='flex-end'
              alignItems='flex-start'>
              <Grid
                item
                md={12}
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                {' '}
                <h4 className='card-title'>List de réservations</h4>
                <PDFDownloadLink
                  document={
                    <PdfDocument
                      Data={listBookings.filter((item) => {
                        if (rental != null) {
                          return item.links.rental === rental;
                        }
                      })}
                      Rental={rental}
                      Source={sources}></PdfDocument>
                  }
                  fileName='Facture'>
                  {({ loading }) =>
                    loading ? (
                      <Button color='error' variant='contained'>
                        loading Document...
                      </Button>
                    ) : (
                      <Button color='success' variant='contained'>
                        Export to pdf
                      </Button>
                    )
                  }
                </PDFDownloadLink>
              </Grid>
              {/* <Grid item md={6}>
               
              </Grid> */}
            </Grid>
            <div className='header-title'></div>
          </Card.Header>
          <Card.Body className='px-0'>
            {({ listBookings }) =>
              listBookings.length > 0 ? <div>full</div> : <div>empty</div>
            }
            <div className='table-responsive'>
              <table
                style={{ width: 400 }}
                id='user-list-table'
                className='table table-striped'
                role='grid'
                data-toggle='data-table'>
                <thead>
                  <tr className='ligth'>
                    <th>Date Début</th>
                    <th>Date Fin</th>
                    <th>Status</th>
                    <th style={{ width: '5%', wordBreak: 'break-all' }}>
                      Nombre Adultes
                    </th>
                    <th>Prix Finale</th>
                    <th>Montant payeé</th>
                    <th>Montant a payée</th>
                  </tr>
                </thead>
                <tbody>
                  {listBookings.map((item) => (
                    <tr key={item.id}>
                      <td>{item.start_at.substr(0, 10)}</td>
                      <td>{item.end_at.substr(0, 10)}</td>
                      <td>{item.status}</td>
                      <td>{item.adults}</td>
                      <td>{item.final_price}</td>
                      <td>{item.paid_amount}</td>
                      <td>{item.payment_left_to_collect}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </Card.Body>
        </Card>
      </Grid>
      <Grid item xs={12}>
        <PDFViewer>
          <PdfDocument
            Data={listBookings.filter((item) => {
              if (rental != null) {
                return item.links.rental === rental;
              }
            })}
            Rental={rental}></PdfDocument>
        </PDFViewer>
      </Grid>
    </Grid>
  );
}

export default Details;
