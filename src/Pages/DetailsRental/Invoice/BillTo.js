/** @format */

import React from 'react';
import { Text, View, StyleSheet } from '@react-pdf/renderer';

const styles = StyleSheet.create({
  headerContainer: {
    position: 'relative',
    top: '-400px',
    justifyContent: 'flex-start',
    width: '100%',
  },
  billTo: {
    paddingBottom: 3,
    fontFamily: 'Helvetica-Oblique',
    fontWeight: 'bold',
  },
});

const BillTo = ({ invoice }) => (
  <View style={styles.headerContainer}>
    <Text style={styles.billTo}>Bill To:</Text>
    <Text>HSH RENTAL</Text>
    <Text>81 rue d’Amsterdam ,75008 Paris</Text>
    <Text>contact@homesuitehome.fr</Text>
    <Text>SIREN: 891 730 848</Text>
  </View>
);

export default BillTo;
