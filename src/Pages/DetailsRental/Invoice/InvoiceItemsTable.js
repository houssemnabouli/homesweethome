/** @format */

import React from 'react';
import { View, StyleSheet } from '@react-pdf/renderer';
import InvoiceTableHeader from './InvoiceTableHeader';
import InvoiceTableRow from './InvoiceTableRow';
import InvoiceTableFooter from './InvoiceTableFooter';

const styles = StyleSheet.create({
  tableContainer: {
    flexDirection: 'column',
    flexWrap: 'wrap',
    margin: '20px',
    // borderWidth: 1,
    // borderColor: '#3778C2',
    width: '100%',

    alignItems: 'flex-start',
    top: '-250px',
    position: 'relative',
    height: '400px',
    justifyContent: 'space-around',
  },
});

const InvoiceItemsTable = (props) => (
  <View style={styles.tableContainer}>
    <View>
      <InvoiceTableHeader />
      <InvoiceTableRow
        Data={props.Data}
        Rental={props.Rental}
        Fees={props.Fees}
      />
    </View>
    <View>
      <InvoiceTableFooter
        style={{ position: 'relative', bottom: '0px' }}></InvoiceTableFooter>
    </View>
  </View>
);

export default InvoiceItemsTable;
