/** @format */

import React, { useEffect, useState } from 'react';
import {
  Page,
  Document,
  StyleSheet,
  Image,
  Text,
  View,
} from '@react-pdf/renderer';
import { Get_Rental_ById_Include } from '../../../api/action_rentals';
import datacomp from '../../../data/DataFee.json';
import logo from '../../../images/fav.png';
import InvoiceTitle from './InvoiceTitle';
import InvoiceNo from './InvoiceNo';
import BillTo from './BillTo';
import InvoiceThankYouMsg from './InvoiceThankYouMsg';
import InvoiceItemsTable from './InvoiceItemsTable';
import InvoiceData from '../Invoice/JsonData/generated.json';
const styles = StyleSheet.create({
  table: {
    display: 'table',
    width: '100%',
    borderStyle: 'solid',
    borderWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0,
  },
  tableRow: {
    margin: 'auto',
    flexDirection: 'row',
    borderWidth: 1,
    fontSize: '3px',
  },
  tableRowMain: {
    margin: 'auto',
    flexDirection: 'row',
    borderWidth: 1,
    fontSize: '3px',
    backgroundColor: '#3778C2',
    color: '#fff',
  },
  tableCol: {
    width: '12%',
    borderStyle: 'solid',
    borderWidth: 0,
    borderLeftWidth: 0,
    borderTopWidth: 0,
  },
  tableCell: {
    margin: 'auto',
    marginTop: 5,
    fontSize: '9px',
    fontWight: 'bold',
    padding: '5px',
    AlignText: 'center',
  },
  page: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '2%',
  },
  logo: {
    width: 84,
    height: 70,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  MainHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    fontFamily: 'Helvetica',
    fontSize: '9px',
    width: '100%',
    flexDirection: 'row',
    padding: '5%',
  },
  ContentHeader: {
    textAlign: 'justify',
  },
});

// const styles = StyleSheet.create({
//   page: {
//     backgroundColor: '#fff',
//     fontFamily: 'Helvetica',
//     fontSize: 11,
//     paddingTop: 30,
//     paddingLeft: 20,
//     paddingRight: 50,
//     lineHeight: 1.5,
//     flexDirection: 'column',
//   },
//   logo: {
//     width: 84,
//     height: 70,
//     marginLeft: 'auto',
//     marginRight: 'auto',
//   },
// });

const InvoiceHeader = (props) => (
  <div style={styles.MainHeader}>
    <View style={styles.ContentHeader}>
      <Text>HSH RENTAL</Text>
      <Text>81 rue d’Amsterdam</Text>
      <Text>75008 Paris</Text>
      <Text>06.13.31.13.83</Text>
      <Text>contact@homesuitehome.fr</Text>
      <Text>SIREN: 891 730 848</Text>
    </View>
    <View style={styles.ContentHeader}>
      <Text>Facture HSH {new Date().toISOString().substring(0, 10) + '6'}</Text>
      <Text>Date: {new Date().toISOString().substring(0, 10)} </Text>
      <Text>Appartement: {props?.name?.substring(5, props.name.length)}</Text>
      <Text>Equipement Bâtiments Rénovations</Text>
      <Text>{props?.address1}</Text>
      <Text>
        {props?.zip} {props?.city}
      </Text>
    </View>
  </div>
);

const PdfDocument = (props) => {
  const [rent, setRent] = useState();
  const [total_nb_nuit, set_total_nb_nuit] = useState(0);
  const [total_montant, set_total_montant] = useState(0);
  const [total_cleaning, set_total_cleaning] = useState(0);
  const [total_comission, set_total_comission] = useState(0);
  let cle_fee = 0;
  const Get_Cleaning_Fee = (rental) => {
    let x = datacomp.filter((a) => {
      if (a.Reference.substring(0, 5) === rental.name.substring(0, 5)) {
        return a;
      }
    });
    // set_total_cleaning(total_cleaning + parseInt(x[0].Prix_Menage_Facture_TTC));
    return x[0].Prix_Menage_Facture_TTC;
  };

  const Calculate_tax = (booking) => {
    let sum = 0;
    let filtred = booking.bookings_fees.filter((a) => {
      if (a.name.en.includes('Tourism')) {
        return a.price;
      }
    });
    return filtred;
  };
  let total_montant_enc = [];

  const Generate_Montant_Encaisse_Booking = (rental, booking, index) => {
    let sum = 0;
    let taxes = Calculate_tax(booking);
    console.log('booking', booking);
    // console.log('taxes', taxes.length > 0 && taxes);
    for (let i = 0; i < taxes.length; i++) {
      //log(taxes[i].price, 'price');
      sum = sum + parseInt(taxes[i].price, 10);
    }

    let paymentcharge = (parseInt(booking.paid_amount, 10) * 0.11) / 10;
    console.log('summ', sum);

    let result = parseInt(booking.paid_amount, 10) - sum - paymentcharge;
    // total_montant_enc = total_montant_enc + parseInt(result);
    // set_total_montant(total_montant + result);
    console.log(
      result,
      'resulat',
      index,
      parseInt(booking.paid_amount, 10),
      paymentcharge,
      sum
    );
    total_montant_enc.push(result);
    return result;
  };

  const Generate_Montant_Encaisse_Booking_airbnb = (rental, booking, index) => {
    let sum = 0;
    let taxes = Calculate_tax(booking);
    console.log('booking', booking);
    // console.log('taxes', taxes.length > 0 && taxes);
    for (let i = 0; i < taxes.length; i++) {
      //log(taxes[i].price, 'price');
      sum = sum + parseInt(taxes[i].price, 10);
    }

    // let paymentcharge = (parseInt(booking.paid_amount, 10) * 0.11) / 10;
    // console.log('summ', sum);

    let result =
      parseFloat(booking.paid_amount, 10).toFixed(2) -
      sum -
      parseFloat(booking.commission).toFixed(2);
    // total_montant_enc = total_montant_enc + parseInt(result);
    // set_total_montant(total_montant + result);

    total_montant_enc.push(result);
    return result - 1;
  };
  let total_nb_nuits = 0;

  // useEffect(() => {
  //   console.log(props.Data, 'dataso');
  // }, [props.Data]);
  function getDifferenceInDays(start, end) {
    const date1 = new Date(start);
    const date2 = new Date(end);

    const diffInMs = Math.abs(date2 - date1);
    let final = diffInMs / (1000 * 60 * 60 * 24);
    // set_total_nb_nuit(total_nb_nuit + (final - 1));
    total_nb_nuits = total_nb_nuits + final;

    return final;
  }
  let Montant_Soumis_Commission = [];
  const Generate_Montant_Soumis_Commission = (item) => {
    let fee = parseFloat(
      Generate_Montant_Encaisse_Booking(rent, item) - Get_Cleaning_Fee(rent)
    ).toFixed(2);
    Montant_Soumis_Commission.push(fee);
    // set_total_comission(total_comission + fee);

    return fee;
  };

  useEffect(() => {
    if (Montant_Soumis_Commission.length > 0) {
      let unique = [...new Set(Montant_Soumis_Commission)];
      if (unique.length === props.Data.length) {
        set_total_montant(
          unique.reduce((a, b) => {
            return parseFloat(a) + parseFloat(b);
          })
        );
      }
    }
  });

  useEffect(() => {
    // console.log('comission_total', comission_total);
    if (comission_total.length > 0) {
      let unique = [...new Set(comission_total)];
      if (unique.length === props.Data.length) {
        set_total_comission(
          unique.reduce((a, b) => {
            return parseFloat(a) + parseFloat(b);
          })
        );
      }
    }
  });

  const Calculate_Msoumis_somme = () => {
    const unique = [...new Set(Montant_Soumis_Commission)];

    // [1, 2, 3, 5, 8, 9, 4]

    // for (let i = 0; i < props?.Data.length; i++) {
    //   if ([12774, 12822].indexOf(props?.data[i].links.source) !== -1) {
    //   }
    // }

    set_total_montant(unique);

    return unique.reduce((a, b) => {
      return a + b;
    });
  };

  const Calculate_enc_somme = () => {
    const unique = [...new Set(total_montant_enc)];

    // [1, 2, 3, 5, 8, 9, 4]

    // for (let i = 0; i < props?.Data.length; i++) {
    //   if ([12774, 12822].indexOf(props?.data[i].links.source) !== -1) {
    //   }
    // }
    return unique.reduce((a, b) => {
      return a + b;
    });
  };
  let comission_total = [];
  const generate_comission_total = (item, index) => {
    let comission =
      parseFloat(
        Generate_Montant_Encaisse_Booking(rent, item, index) -
          Get_Cleaning_Fee(rent)
      ).toFixed(2) / 5;
    comission_total.push(comission);
    return comission;
  };

  let com_sum_calculate = () => {
    const unique = [...new Set(comission_total)];

    // [1, 2, 3, 5, 8, 9, 4]

    // for (let i = 0; i < props?.Data.length; i++) {
    //   if ([12774, 12822].indexOf(props?.data[i].links.source) !== -1) {
    //   }
    // }
    return unique.reduce((a, b) => {
      return a + b;
    });
  };

  // useEffect(() => {
  //   console.log('total_comission', total_comission);
  // }, [total_comission]);

  useEffect(() => {
    Get_Rental_ById_Include(props.Rental).then((res) => {
      setRent(res.data?.rentals[0]);
    });
  }, [props.Rental]);
  return (
    <Document>
      <Page>
        <Image style={styles.logo} src={logo} />
        <InvoiceTitle title={'HomeSweetHome'} />
        {InvoiceHeader(rent)}
        <View style={styles.page}>
          <View style={styles.table}>
            <View style={styles.tableRowMain}>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>Plateforme</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>Date d’entrée</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>Nb nuit</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>Montant total encaissé</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>Ménage</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>Montant ménage en €</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>
                  Montant soumis à commission en €
                </Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>Commission En € TTC</Text>
              </View>
            </View>
            {props?.Data.map((item, index) => {
              if ([12774, 12822].indexOf(item.links.source) !== -1) {
                return (
                  <View style={styles.tableRow}>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>
                        {item.links.source === 12774 ? 'Airbnb' : 'Booking'}
                      </Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>
                        {item.start_at.substr(0, 10)}{' '}
                      </Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>
                        {getDifferenceInDays(
                          item.start_at.substr(0, 10),
                          item.end_at.substr(0, 10)
                        )}
                      </Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>
                        {item && item.links.source === 12774
                          ? Generate_Montant_Encaisse_Booking_airbnb(
                              rent,
                              item,
                              index
                            ) + '€'
                          : Generate_Montant_Encaisse_Booking(
                              rent,
                              item,
                              index
                            ) + '€'}
                      </Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>{'HSH'}</Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>
                        {rent && Get_Cleaning_Fee(rent)}
                        {'€'}
                      </Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>
                        {rent && Generate_Montant_Soumis_Commission(item) + '€'}
                      </Text>
                    </View>
                    <View style={styles.tableCol}>
                      <Text style={styles.tableCell}>
                        {' '}
                        {rent &&
                          parseFloat(
                            generate_comission_total(item, index)
                          ).toFixed(2)}
                        {'€'}
                      </Text>
                    </View>
                  </View>
                );
              }
            })}
            <View style={styles.tableRow}>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>Total</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}></Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>{total_nb_nuits}</Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>
                  {total_montant_enc.length > 0 && Calculate_enc_somme() + '€'}
                </Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}></Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>
                  {rent &&
                    parseInt(Get_Cleaning_Fee(rent)) * props.Data.length + '€'}
                </Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>
                  {total_montant && parseFloat(total_montant).toFixed(2) + '€'}
                </Text>
              </View>
              <View style={styles.tableCol}>
                <Text style={styles.tableCell}>
                  {total_comission &&
                    parseFloat(total_comission).toFixed(2) + '€'}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </Page>
    </Document>
  );
};

export default PdfDocument;
