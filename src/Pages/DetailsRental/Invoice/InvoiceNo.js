/** @format */

import React, { Fragment } from 'react';
import { Text, View, StyleSheet } from '@react-pdf/renderer';

const styles = StyleSheet.create({
  invoiceNoContainer: {
    flexDirection: 'row',
    marginTop: 36,
    justifyContent: 'flex-end',
  },
  invoiceDateContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  invoiceDate: {
    fontSize: 12,
    fontWeight: 'bold',
    position: 'relative',
    // left: '-50px',
  },
  label: {
    width: '90px',
    // left: '-50px',

    // marginLeft: '200px',
  },
});
function validate(variable) {
  if (variable.Rental !== undefined) {
    return (
      <Fragment>
        <View style={styles.invoiceNoContainer}>
          <Text style={styles.label}>Facture HSH:</Text>
          <Text style={{ width: '90px', position: 'relative', left: '-20px' }}>
            {event}
          </Text>
        </View>
        <View style={styles.invoiceDateContainer}>
          <Text style={styles.label}>Date:</Text>
          <Text style={{ width: '90px', position: 'relative', left: '-60px' }}>
            {ch}
          </Text>
        </View>
        <View style={styles.invoiceDateContainer}>
          <Text style={styles.label}> {variable.Rental.rental_type}</Text>
          <Text style={styles.invoiceDate}>{variable.Rental.city}</Text>
        </View>
        <View style={styles.invoiceDateContainer}>
          <Text style={styles.invoiceDate}>{variable.Rental.address1}</Text>
        </View>
      </Fragment>
    );
  } else {
    return <div></div>;
  }
}
const date = new Date();
const event = date.toISOString().substr(0, 10);
const ch = date.toDateString();
const InvoiceNo = (Rental) => validate(Rental);

export default InvoiceNo;
