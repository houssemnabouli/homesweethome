/** @format */

import React, { Fragment, useState } from 'react';
import { Text, View, StyleSheet } from '@react-pdf/renderer';
import { List } from '@mui/material';
import axios from 'axios';

const borderColor = '#3778C2';
const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    borderBottomColor: '#3778C2',
    borderBottomWidth: 1,
    // alignItems: 'center',
    // height: 24,
    fontStyle: 'bold',
    fontSize: '9px',
    alignItems: 'flex-start',
    height: 'auto',
    textAlign: 'center',
    // fontStyle: 'bold',
    flexGrow: 1,
    // fontSize: '7px',
    display: 'flex',
    justifyContent: 'flex-start',
  },
  description: {
    width: '13%',
    borderRightColor: borderColor,
    borderRightWidth: 1,
    height: '100%',
    // paddingLeft: 8,
  },
  qty: {
    width: '13%',
    borderRightColor: borderColor,
    borderRightWidth: 1,
    height: '100%',
    // paddingRight: 8,
  },
  rate: {
    width: '13%',
    borderRightColor: borderColor,
    borderRightWidth: 1,
    height: '100%',
    // paddingRight: 8,
  },
  amount: {
    width: '13%',
    borderRightColor: borderColor,
    borderRightWidth: 1,
    height: '100%',
    // paddingRight: 8,
  },
});
const sumTotale = 0;
function getSource(ch) {
  const x = 'airbnb';
  return x;
}
function getnuit(chS, chE) {
  const x = chS.split('-');
  const y = chE.split('-');
  const dateS = new Date(x[0], x[1] - 1, x[2]);
  const dateE = new Date(y[0], y[1] - 1, y[2]);
  return Math.round((dateE - dateS) / (1000 * 60 * 60 * 24));
}

function calculer(FeeId, listFees) {
  const sum = 0;
  //  console.log("FeeId",FeeId)
  // console.log("List",listFees)

  listFees.forEach((item) => {
    if (item.id == FeeId) {
      sum = sum + item.price;
    }
  });

  return sum;
}
const InvoiceTableRow = (items) => {
  let Tokenref = {
    access_token:
      '6415ae71bdfe40ad482d57165defafccc8c04861a96a973e0979655b050751af',
    id: '',
  };
  const [Fee, setFee] = useState(0);
  const list = ['Clean', 'Cleaning', 'menage', 'ménage'];
  function CalculCleaningFee(listFeeId) {
    let sum = 0;
    let i = 0;
    listFeeId.forEach((item) => {
      if (item.name.en.toLowerCase().includes('cleaning'))
        sum = sum + item.price;
      i = i + 1;
    });
    return sum;
  }

  // const generatedrows = ()=>{

  // return <View style={styles.row}>
  // items.Data.map((item) => (

  // </View>;

  // }

  const rows = items.Data.map((item) => (
    <View style={styles.row} key={item.id}>
      <Text style={styles.amount}>{getSource(items.Rental)}</Text>
      <Text style={styles.amount}>{item.start_at.substr(0, 10)}</Text>
      <Text style={styles.amount}>
        {getnuit(item.start_at.substr(0, 10), item.end_at.substr(0, 10))}
      </Text>
      <Text style={styles.amount}>{item.final_price}</Text>
      <Text style={styles.amount}>{CalculCleaningFee(item.bookings_fees)}</Text>
      <Text style={styles.amount}>{item.commission}</Text>
      <Text style={styles.amount}>test</Text>
      <Text style={styles.amount}>test</Text>
      <Text style={styles.amount}>test</Text>
    </View>
  ));

  return <Fragment>{rows}</Fragment>;
};

export default InvoiceTableRow;
