/** @format */

import React from 'react';
import { Text, View, StyleSheet } from '@react-pdf/renderer';

const borderColor = '#fff';
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderBottomColor: '#fff',
    backgroundColor: '#3778C2',
    color: '#fff',
    borderBottomWidth: 1,
    alignItems: 'flex-start',
    height: 'auto',
    textAlign: 'center',
    fontStyle: 'bold',
    flexGrow: 1,
    fontSize: '7px',
    display: 'flex',
    justifyContent: 'flex-start',
  },
  Plateforme: {
    width: '11.8%',
    borderRightColor: borderColor,
    borderRightWidth: 1,
    height: '100%',
  },
  date: {
    width: '11.8%',
    borderRightColor: borderColor,
    borderRightWidth: 1,
    height: '100%',
  },
  Nb: {
    width: '11.8%',
    borderRightColor: borderColor,
    borderRightWidth: 1,
    height: '100%',
  },
  amount: {
    width: '11.8%',
    height: '100%',
    borderRightColor: borderColor,
    borderRightWidth: 1,
  },
  Menage: {
    width: '11.8%',
    height: '100%',
    borderRightColor: borderColor,
    borderRightWidth: 1,
  },
});

const InvoiceTableHeader = () => (
  <View style={styles.container}>
    <Text style={styles.Plateforme}>Plateforme</Text>
    <Text style={styles.date}>Date d’entrée</Text>
    <Text style={styles.Nb}>Nb nuit</Text>
    <Text style={styles.amount}>Montant total encaissé</Text>
    <Text style={styles.Menage}>Ménage</Text>
    <Text style={styles.amount}>Montant ménage en €</Text>
    <Text style={styles.amount}>Montant soumis à commission en €</Text>
    <Text style={styles.amount}>CommissionEn € TTC</Text>
  </View>
);

export default InvoiceTableHeader;
