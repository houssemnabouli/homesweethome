//datepicker
import { Datepicker } from 'vanillajs-datepicker';
import React, { useEffect, useState } from 'react';
import { DateRangePicker } from 'vanillajs-datepicker';
import '../../../node_modules/vanillajs-datepicker/dist/css/datepicker.min.css';
const Datepickers = (props) => {
console.log(props)
    useEffect(
        () =>{
            const datepickers = document.querySelectorAll('.vanila-datepicker')
            Array.from(datepickers, (elem) => {
                return new Datepicker(elem)
            })
            const daterangePickers = document.querySelectorAll('.vanila-daterangepicker')
            Array.from(daterangePickers, (elem) => {
            return new DateRangePicker(elem)
            })
        
           
        },[]
    )
  
    return (
        <>
         <input type="text" name={props.name} className="vanila-datepicker" placeholder="Date Picker" />        </>
    )
}
export default Datepickers;
