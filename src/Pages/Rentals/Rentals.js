/** @format */
import listJson from '../../assets/Rentals.json';

import React, { useEffect, useState, CSSProperties } from 'react';
import ClipLoader from 'react-spinners/ClipLoader';

import { Form } from 'react-bootstrap';
import axios from 'axios';
import Card from '@mui/material/Card';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import { useHistory } from 'react-router-dom';
import Grid from '@mui/material/Grid';
import { Get_All_RentalsByPage } from '../../api/action_rentals';
import { List } from '@mui/material';
import house from '../../assets/images/house.jpg'
function Rentals() {
  let history = useHistory();
  const [list, setList] = useState([]);
  const [TotalePages, setTotalePages] = useState('1');
  const [Page, setPage] = useState('1');
  const [select, SetSelect] = useState('0');
  const [surface, setSurface] = useState(0);
  const [nameFilter, setNameFilter] = useState('');
  const [cityFilter, setCityFilter] = useState('');
  const [newList, setNewList] = useState([]);
  const [FullList, setFullList] = useState([]);
  const [Start, setStart] = useState(true);
  let [loading, setLoading] = useState(true);
  let [color, setColor] = useState('blue');

  useEffect(() => {
    Get_All_RentalsByPage(1, '').then((res) => {
      setList(res.data.rentals);
      setLoading(false);
      let aux = [];
      let aux2 = [];
      let max = '';
      max = parseInt(
        res.data.meta.Link.last.substring(
          res.data.meta.Link.last.indexOf('page=') + 5,
          res.data.meta.Link.last.indexOf('page=') + 6
        )
      );
      setTotalePages(max);
      Array.prototype.push.apply(aux2, res.data.rentals);
      aux.push(res.data.rentals);
      if (max > 1) {
        for (let i = 2; i < max + 1; i++) {
          Get_All_RentalsByPage(i, '').then((res) => {
            aux.push(res.data.rentals);
            Array.prototype.push.apply(aux2, res.data.rentals);
          });
        }
      }

      setFullList(aux2);
      setNewList(aux);
      setStart(true);
    });
  }, []);
  // console.log(newList )
  // useEffect(() => {
  //  // console.log(FullList)
  //   if (FullList.length > 0) {

  //   }
  // }, [])
  const handleChange = (event) => {
    SetSelect(event.target.value);
  };
  function getSimilair(reference) {
    let x = '';
    FullList.forEach((bien) => {
      if (bien.name != reference) {
        //  console.log(bien.name)
        return bien.name;
      }
    });
  }
  const handePage = (event, value) => {
    setPage(value);
    let aux = [];
    let s = parseInt(newList.length / TotalePages);
    if (newList.length != 0 && Start) {
      console.log(list);
      setList([]);
      setList(newList[value - 1]);
      console.log(list);

      // listJson.forEach((item) => {
      // //  console.log(getSimilair(item))
      //   aux.push(getSimilair(item))

      // })
    }
    //   console.log(aux)
  };
  //Change Page
  console.log(newList);
  const handleSubmit = (event) => {
    SetSelect('0');
    setNameFilter('');
    setSurface(0);
    setCityFilter('');
  };
  const handleDetails = (item) => {
    history.push('/dashboard/app/details', { state: { rental: item.id } });
  };

  return (
    <>
      <Grid container sx={{ my: 5 }} spacing={2}>
        <Grid item md={12} xs={12} sx={{}}>
          <Card sx={{ width: '100', height: '100' }}>
            <Grid container spacing={2} sx={{ ml: 2, my: 2, mr: 3 }}>
              <Grid item md={3}>
                <div class='form-floating mb-3'>
                  <input
                    type='texte'
                    class='form-control'
                    id='floatingInput'
                    value={nameFilter}
                    onChange={(e) => {
                      setNameFilter(e.target.value);
                    }}
                    placeholder='name@example.com'></input>
                  <label for='floatingInput'>Nom de location</label>
                </div>
              </Grid>

              <Grid item md={3}>
                <div class='form-floating mb-3'>
                  <input
                    type='texte'
                    class='form-control'
                    id='floatingInput'
                    value={cityFilter}
                    onChange={(e) => {
                      setCityFilter(e.target.value);
                    }}
                    placeholder='name@example.com'></input>
                  <label for='floatingInput'>recherche par nom de ville </label>
                </div>
              </Grid>

              <Grid item md={12}>
                <button
                  type='button'
                  onClick={handleSubmit}
                  class='btn btn-primary'>
                  Clear
                </button>
              </Grid>
            </Grid>
          </Card>
        </Grid>

        <Grid item md={12} xs={12}>
          {!loading ? (
            <Grid container sx={{ mt: 5 }} spacing={3}>
              {list
                .filter((item) => {
                  return item.name
                    .toLowerCase()
                    .includes(nameFilter.toLowerCase());
                })
                .filter((item) => {
                  if (item.city != null) {
                    return item.city
                      .toLowerCase()
                      .includes(cityFilter.toLowerCase());
                  } else return item;
                })

                .map((itemFiltered) => (
                  <Grid item md={3}>
                    <div class='card'>
                      <img
                        alt='no image'
                        src={
                          house
                        }
                        class='bd-placeholder-img card-img-top'
                        width='100%'
                        height='180'
                        xmlns='http://www.w3.org/2000/svg'
                        role='img'
                        aria-label='Placeholder: Image cap'
                        preserveAspectRatio='xMidYMid slice'
                        focusable='false'></img>{' '}
                      <div class='card-body'>
                        <h5 class='card-title'>{itemFiltered.name}</h5>
                        <p class='card-text'>{itemFiltered.summary.fr}</p>
                        <button
                          type='button'
                          onClick={() => {
                            handleDetails(itemFiltered);
                          }}
                          class='btn btn-primary'>
                          Details
                        </button>
                      </div>
                    </div>
                  </Grid>
                ))}
              <Grid item md={12}>
                <Stack
                  spacing={2}
                  direction='row'
                  justifyContent='center'
                  alignItems='center'>
                  <Pagination
                    page={Page}
                    onChange={handePage}
                    count={TotalePages}
                    color='primary'
                  />
                </Stack>
              </Grid>
            </Grid>
          ) : (
            <div
              style={{
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                height: '70vh',
              }}>
              <ClipLoader
                color={color}
                loading={loading}
                size={150}
                aria-label='Loading Spinner'
                data-testid='loader'
              />
            </div>
          )}
        </Grid>
      </Grid>
    </>
  );
}

export default Rentals;
