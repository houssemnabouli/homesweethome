/** @format */

const express = require('express');
const router = express.Router();
const axios = require('axios');
router.post('/GET_Token_Auth', (req, res) => {
  //   res.send('api_working');
  // let Tokenref = {
  //   client_id: 'b29c8d9317ddcf8588fe72b4cda8dc87d7cc9086369df51e3737ba5c2e05bcd0',
  //   client_secret:
  //     '8d979548361d76daa298c9175a79794d88c1d9ba7a32ec2d8185bde0bbc0aa94',
  //   code: req.body.authcode,
  //   grant_type: 'authorization_code',
  //   redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
  // };

  axios
    .post('https://www.bookingsync.com/oauth/token', req.body)
    .then((result) => {
      const config = {
        headers: { Authorization: `Bearer ${result.data.access_token}` },
      };
      axios
        .get('https://www.bookingsync.com/api/v3/accounts', config)
        .then((Account) => {
          console.log( result.data);
          res.json({ account: Account.data.accounts[0], token: result.data });
        })
        .catch((Account_error) => {
          console.log(Account_error);
        });
    })
    .catch((err) => console.error(err));
});

router.get('/test', (req, res) => {
  res.send('api_working');
});
module.exports = router;
