/** @format */

const express = require('express');
const router = express.Router();
const axios = require('axios');
router.post('/Get_Rentals', (req, res) => {
  const config = {
    headers: { Authorization: `Bearer ${req.body.access_token}` },
  };
  axios
    .get('https://www.bookingsync.com/api/v3/rentals', config)
    .then((Account) => {
      console.log(Account.data);
      res.json({ Rentals: Account.data.rentals, token: Account.data });
    })
    .catch((Account_error) => {
      console.log(Account_error);
    });
});
router.post('/Get_Photos', (req, res) => {
  const config = {
    headers: { Authorization: `Bearer ${req.body.access_token}` },
  };
  axios
    .get('https://www.bookingsync.com/api/v3/photos', config)
    .then((Account) => {
      
      res.json({ Rentals: Account.data.photos, token: Account.data });
    })
    .catch((Account_error) => {
      console.log(Account_error);
    });
});

router.get('/test', (req, res) => {
  res.send('api_working');
});
module.exports = router;
