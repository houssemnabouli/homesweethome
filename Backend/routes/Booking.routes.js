/** @format */

const express = require('express');
const router = express.Router();
const axios = require('axios');
const { data } = require('jquery');
router.post('/get_volumes_affaires_brut', (req, res) => {
  //   res.send('api_working');
  // let Tokenref = {
  //   client_id: 'b29c8d9317ddcf8588fe72b4cda8dc87d7cc9086369df51e3737ba5c2e05bcd0',
  //   client_secret:
  //     '8d979548361d76daa298c9175a79794d88c1d9ba7a32ec2d8185bde0bbc0aa94',
  //   code: req.body.authcode,
  //   grant_type: 'authorization_code',
  //   redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
  // };

  //   axios
  //     .post('https://www.bookingsync.com/oauth/token', req.body)
  //     .then((result) => {
  const config = {
    headers: { Authorization: `Bearer ${req.body.access_token}` },
  };
  axios
    .get('https://www.bookingsync.com/api/v3/bookings', config)
    .then((bookings) => {
      let datenow = new Date();
      let yesterday = new Date();
      yesterday.setDate(datenow.getDate() - 1);

      //  console.log(bookings.data);
      let filtredb = [];
      let sum = 0;
      let CommissionTotale = 0;
      let fee_range = ['Clean', 'Cleaning', 'menage', 'ménage'];
      let fee_sum = [];

      bookings.data.bookings.map((bookin, index) => {
        if (
          bookin.booked_at.substr(0, 10) ===
          yesterday.toISOString().substr(0, 10)
        ) {
          filtredb.push(bookin);
          sum = sum + parseInt(bookin.paid_amount, 10);

          CommissionTotale =
            parseInt(bookin.commission, 10) === parseInt(bookin.commission, 10)
              ? parseInt(bookin.commission, 10) + CommissionTotale
              : 0 + CommissionTotale;
        }
      });
      //   filtredb.map((reservation, indexres) => {
      //     reservation?.links?.bookings_fees.map(async (fee, feeindex) => {
      //       let feedata = await axios
      //         .get(
      //           'https://www.bookingsync.com/api/v3/bookings_fees/' + fee,
      //           config
      //         )
      //         .then((res) => {
      //           fee_sum.push(res.data);
      //         });

      //       //   let iscleaningfee = fee_range.map((w) => {
      //       //     if (
      //       //       feedata.data.bookings_fees[0].name.en
      //       //         .toLowerCase()
      //       //         .includes(w.toLowerCase()) == true
      //       //     ) {
      //       //       return true;
      //       //     } else {
      //       //       return false;
      //       //     }
      //       //   });

      //       //   if (iscleaningfee.includes(true)) {
      //       //     sum =
      //       //       parseInt(feedata.data.bookings_fees[0]?.price, 10) !==
      //       //       parseInt(feedata.data.bookings_fees[0]?.price, 10)
      //       //         ? fee_sum.push(0)
      //       //         : fee_sum.push(parseInt(feedata.data.bookings_fees[0]?.price));
      //       //   }

      //       //   if (index === filtredb.length) {
      //       //   }
      //     });
      //     console.log(fee_sum, 'fee_sum');
      //   });
      //   console.log(fee_sum, 'fee_sum_out');

      return res.json({
        bookinglist: filtredb,
        totalbrut: sum,
        totalnet: sum - CommissionTotale,
        bookingstotal: bookings.data.bookings,
      });
    })
    .then(() => {})
    .catch((Account_error) => {
      console.log(Account_error);
    });
  //     })
  //     .catch((err) => console.error(err));
  //   const date = new Date();
  //   let fromdate = new Date();
  //   fromdate.setHours(1, 0, 0, 0);
  //   res.send({
  //     date_brut: date,
  //     formatted: fromdate.toISOString().substr(0, 10),
  //   });
});

router.post('/get_cleaning_fee', (req, res) => {
  const config = {
    headers: { Authorization: `Bearer ${req.body.access_token}` },
  };
  let feesum = [];
  // reservation?.links?.bookings_fees.map(async (fee, feeindex) => {
  let feetable = [];
  axios
    .get(
      'https://www.bookingsync.com/api/v3/bookings_fees/' + req.body.feeid,
      config
    )
    .then((result) => {
      // console.log(result.data);
      feetable.push(result.data);
      return res.send(result.data);
    });
});

router.post('/get_data_by_month', (req, res) => {
  const config = {
    headers: { Authorization: `Bearer ${req.body.access_token}` },
  };
  axios
    .get('https://www.bookingsync.com/api/v3/bookings', config)
    .then((bookings) => {
      res.send(bookings);
      let datenow = new Date();
      let yesterday = new Date();
      yesterday.setDate(datenow.getDate() - 1);

      //  console.log(bookings.data);
      let filtredb = [];
      let sum = 0;
      let CommissionTotale = 0;
      let fee_range = ['Clean', 'Cleaning', 'menage', 'ménage'];
      let fee_sum = [];
      let currentMonth = new Date().getMonth() + 1;
      let currentYear = new Date().getFullYear();

      bookings.data.bookings.map(async (bookin, index) => {
        res.send(
          await bookings.data.bookings.filter((e) => {
            var [year, month] = bookin.booked_at.substr(0, 10).split('-');
            // Or, var month = e.date.split('-')[1];
            console.log(currentMonth === +month && currentYear == year);
            return currentMonth === +month && currentYear == year;
          })
        );
        // bookings.data.bookings.length - 1 === index && events);

        // if (
        //   bookin.booked_at.substr(0, 10) ===
        //   yesterday.toISOString().substr(0, 10)
        // ) {
        //   filtredb.push(bookin);
        //   sum = sum + parseInt(bookin.paid_amount, 10);

        //   CommissionTotale =
        //     parseInt(bookin.commission, 10) === parseInt(bookin.commission, 10)
        //       ? parseInt(bookin.commission, 10) + CommissionTotale
        //       : 0 + CommissionTotale;
        // }
      });

      //   return res.json({
      //     events: events,
      //     totalbrut: sum,
      //     totalnet: sum - CommissionTotale,
      //   });
    })
    .then(() => {})
    .catch((Account_error) => {
      console.log(Account_error);
    });
});
router.post('/Get_bookings', (req, res) => {
  const config = {
    headers: { Authorization: `Bearer ${req.body.access_token}` },
  };
  axios
    .get('https://www.bookingsync.com/api/v3/bookings/', config)
    .then((Account) => {
      console.log(Account);
      res.json({ Bookings: Account.data, token: Account.data });
    })
    .catch((Account_error) => {
      console.log(Account_error);
    });
});
router.post('/get_Bookings_fee', (req, res) => {
  const config = {
    headers: { Authorization: `Bearer ${req.body.access_token}` },
  };

  axios
    .get(
      'https://www.bookingsync.com/api/v3/bookings_fees/' + req.body.id,
      config
    )
    .then((result) => {
      res.json({ bookings_fees: result.data, token: result.data });
    })
    .catch((err) => {
      console.log(err);
    });
});
module.exports = router;
