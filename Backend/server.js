/** @format */

const express = require('express');
const cors = require('cors');
const AuthRoute = require('./routes/Auth.routes');
const BookingRoute = require('./routes/Booking.routes');
const RentalsRoute = require('./routes/Rentals.routes');
const app = express();

// middlewares Express

app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

// Routes
app.use('/server/api/v1/auth', AuthRoute);
app.use('/server/api/v1/Booking', BookingRoute);
app.use('/server/api/v1/Rentals', RentalsRoute);
app.listen(4000, () => {
  console.log(` App listening at http://localhost:${4000} `);
});
